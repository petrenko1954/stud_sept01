#Листинг 8.31 8.33: Добавление метода authenticated? к модели User. app/models/user.rb
#Листинг :8.38 :10.3
#Листинг 10.33: Добавление методов активации пользователя в модель User. app/models/user.rb  :11.10 :12.8 :12.12
class User < ActiveRecord::Base
#March_07 has_many :microposts, dependent: :destroy
 #March_07has_many :active_relationships, class_name:  "Relationship",
 #March_07                                 foreign_key: "follower_id",
#March_07                                  dependent:   :destroy
#March_07has_many :passive_relationships, class_name:  "Relationship",
  #March_07                                 foreign_key: "followed_id",
#March_07                                   dependent:   :destroy
 #March_07 has_many :following, through: :active_relationships,  source: :followed
 #March_07 has_many :followers, through: :passive_relationships, source: :follower

#Листинг 8.32: Добавление метода remember к модели User. ЗЕЛЕНЫЙ app/models/user.rb 
 attr_accessor :remember_token
#attr_accessor :remember_token, :activation_token, :reset_token
  #before_save { self.email = email.downcase }
 before_save   :downcase_email
  before_create :create_activation_digest


  validates :name,  presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  has_secure_password
   validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

  # Возвращает дайджест данной строки.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Возвращает случайный токен.
  def User.new_token
    SecureRandom.urlsafe_base64
  end

  # Запоминает пользователя в базе данных для использования в постоянной сессии.
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  # Возвращает true, если предоставленный токен совпадает с дайджестом.
  def authenticated?(remember_token)
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end
# Забывает пользователя
  def forget
    update_attribute(:remember_digest, nil)
  end

# Активирует аккаунт.
  def activate
    update_attribute(:activated,    true)
    update_attribute(:activated_at, Time.zone.now)
  end

  # Отправляет электронное письмо для активации.
  def send_activation_email
   # UserMailer.account_activation(self).deliver_now
  end

# Устанавливает атрибуты для сброса пароля.
  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest,  User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end
 # Отправляет электронное письмо для сброса пароля.
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

# Определяет прото-ленту. :11.44

  # Возвращает ленту сообщений пользователя. #12.46
  # Полная реализация в "Следовании за пользователями".
  def feed
    #Micropost.where("user_id = ?", id)
 following_ids = "SELECT followed_id FROM relationships
                     WHERE  follower_id = :user_id"
    Micropost.where("user_id IN (#{following_ids})
                     OR user_id = :user_id", user_id: id)  
end
private
     def user_params
           params.require(:user).permit(:name, :email, :password,
                                        :password_confirmation)
     end
    # Переводит адрес электронной почты в нижний регистр.
    def downcase_email
      self.email = email.downcase
    end

    # Создает и присваивает активационнй токен и дайджест.
    def create_activation_digest
      #self.activation_token  = User.new_token
     # self.activation_digest = User.digest(activation_token)
    end

# Возвращает true, если истек срок давности ссылки для сброса пароля .
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end


end

