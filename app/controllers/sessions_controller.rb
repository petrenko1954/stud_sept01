#  Листинг 8.5: Поиск и аутентификация пользователей. app/controllers/#sessions_controller.rb Листинг 8.13 Вход пользователя.
#Листинг 8.11: Внесение модуля Sessions helper в контроллер Application. app/#controllers/application_controller.rb Листинг :8/13 - 8.27-8.34:
#Листинг 9.29:Листинг 10.30: Предотвращение входа неактивированных пользователей. app/controllers/sessions_controller.rb 
class SessionsController < ApplicationController
 protect_from_forgery with: :exception
  include SessionsHelper

  def new
  end

  def create
 user = User.find_by(email: params[:session][:email].downcase)
    #if user && user.authenticate(params[:session][:password])
      #if user.activated?
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        redirect_back_or user
     # else
     #   message  = "Account not activated. "
     #   message += "Check your email for the activation link."
      #  flash[:warning] = message
      #  redirect_to root_url
     # end
   # else
   #   flash.now[:danger] = 'Invalid email/password combination'
   #   render 'new'
   # end
  end



  def destroy
log_out if logged_in?
    redirect_to root_url
  end
end

