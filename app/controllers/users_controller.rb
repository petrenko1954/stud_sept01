class UsersController < ApplicationController
# libr Листинг :9.5 :9.28: :9.33 :9.53 :Sept14
 #before_action :authenticate_user!
 before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy,
                                        :following, :followers]
 
 before_action :correct_user,   only: [:edit, :update]	 
#Листинг 9.54: Предфильтр, ограничивающий доступ к действию destroy. app/controllers/users_controller.rb  :11.22
 
 before_action :admin_user,     only: :destroy
def show
	@user = User.find(params[:id])
	    #@articles = Article.includes(:article_files, :author).all
	    #@conferences = current_user.conferences
#@microposts = @user.microposts.paginate(page: params[:page])
	  end
         #Листинг 7.12: 
def new
		@user = User.new
end
	
def index   #users Листинг 9.5 - 9.33
		@users = User.all
#Листинг 9.42:
 @users = User.paginate(page: params[:page])	
end

#Листинг :7.1  :7.17 # 7.23 7.24 8.22
#:7.16	
def create
     @user = User.new(user_params)
    if @user.save
               flash[:success] = "Welcome to the Sample App!"
           redirect_to @user
       else
      render 'new'
end
	end


 def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end

 #Листинг 9.1: 
def edit
    @user = User.find(params[:id])
  end
		           

 # Предфильтры
#Листинг 9.28: 
    # Подтверждает вход пользователя
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end

# Подтверждает правильного пользователя
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end

 # Подтверждает администратора.
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end

#Листинг 9.5: Первоначальное действие update. app/controllers/users_controller.rb 
def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      # Обрабатывает успешное обновление
    else
      render 'edit'
    end
  end
   end
                   #private

    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end

 end
